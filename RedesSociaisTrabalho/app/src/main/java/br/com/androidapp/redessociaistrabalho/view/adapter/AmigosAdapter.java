package br.com.androidapp.redessociaistrabalho.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.androidapp.redessociaistrabalho.R;
import br.com.androidapp.redessociaistrabalho.app.Constants;
import br.com.androidapp.redessociaistrabalho.model.ObjBasicModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public class AmigosAdapter extends RecyclerView.Adapter<AmigosAdapter.AmigosHolder> {

    private List<ObjBasicModel> list;
    private Context context;
    private AmigosAdapterCallback callback;

    public AmigosAdapter(List<ObjBasicModel> list, AmigosAdapterCallback callback) {
        this.list = list;
        this.callback = callback;
    }

    @Override
    public AmigosHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_amigos, null);
        return new AmigosHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmigosHolder holder, int position) {
        final ObjBasicModel obj = list.get(position);
        holder.txtNomeUsuario.setText(obj.getName());
        Glide.with(context)
                .load(String.format(Constants.URL_IMAGE_FACEBOOK, obj.getId()))
                .bitmapTransform(new CropCircleTransformation(context))
                .into(holder.imgUsuario);

        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (callback != null) callback.selectedAmigo(obj);
                }else{
                    if (callback != null) callback.unSelectedAmigo(obj);
                }
            }
        });

        holder.containerClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.check.isChecked()){
                    holder.check.setChecked(false);
                }else{
                    holder.check.setChecked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class AmigosHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.check)
        CheckBox check;
        @BindView(R.id.img_usuario)
        ImageView imgUsuario;
        @BindView(R.id.txt_nome_usuario)
        TextView txtNomeUsuario;
        @BindView(R.id.container_click)
        LinearLayout containerClick;

        AmigosHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

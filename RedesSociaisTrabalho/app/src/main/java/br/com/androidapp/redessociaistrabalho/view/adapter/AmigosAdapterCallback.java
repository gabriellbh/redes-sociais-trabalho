package br.com.androidapp.redessociaistrabalho.view.adapter;

import br.com.androidapp.redessociaistrabalho.model.ObjBasicModel;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public interface AmigosAdapterCallback {

    void selectedAmigo(ObjBasicModel objBasicModel);
    void unSelectedAmigo(ObjBasicModel objBasicModel);
}

package br.com.androidapp.redessociaistrabalho.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.androidapp.redessociaistrabalho.R;
import br.com.androidapp.redessociaistrabalho.app.Constants;
import br.com.androidapp.redessociaistrabalho.model.ObjBasicModel;
import br.com.androidapp.redessociaistrabalho.model.UserModel;
import br.com.androidapp.redessociaistrabalho.util.Utils;
import br.com.androidapp.redessociaistrabalho.view.adapter.AmigosAdapter;
import br.com.androidapp.redessociaistrabalho.view.adapter.AmigosAdapterCallback;
import br.com.androidapp.redessociaistrabalho.view.adapter.LugaresAdapter;
import br.com.androidapp.redessociaistrabalho.view.adapter.LugaresAdapterCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_usuario)
    ImageView imgUsuario;
    @BindView(R.id.txt_nome_usuario)
    TextView txtNomeUsuario;
    @BindView(R.id.txt_email_usuario)
    TextView txtEmailUsuario;
    @BindView(R.id.lista_lugares)
    RecyclerView listaLugares;
    @BindView(R.id.container_lugares_proximos)
    LinearLayout containerLugaresProximos;
    @BindView(R.id.lista_amigos)
    RecyclerView listaAmigos;
    @BindView(R.id.container_amigos)
    LinearLayout containerAmigos;
    @BindView(R.id.txt_sem_amigos)
    TextView txtSemAmigos;

    private AccessToken token;
    private MaterialDialog materialDialog;
    private ObjBasicModel selectLugar;
    private List<ObjBasicModel> listAmigosSelected = new ArrayList<>();

    public static void open(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        configToolbar();
        createDialog();
        getAccessToken();
        getDadosUsuario();
        getAmigos();
        verifyLocation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_LOCATION) {
            verifyLocation();
        }
    }

    private void configToolbar() {
        setSupportActionBar(toolbar);
    }

    private void createDialog() {
        materialDialog = new MaterialDialog.Builder(this)
                .content("Aguarde")
                .progress(true, 0)
                .build();
    }

    private void getAccessToken() {
        token = AccessToken.getCurrentAccessToken();
    }

    private void getDadosUsuario() {
        materialDialog.show();
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        materialDialog.dismiss();
                        setData((UserModel) Utils.jsonToObject(object.toString(), UserModel.class));
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void setData(UserModel userModel) {
        txtNomeUsuario.setText(userModel.getName());
        txtEmailUsuario.setText(userModel.getEmail());
        Glide.with(this)
                .load(String.format(Constants.URL_IMAGE_FACEBOOK, userModel.getId()))
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imgUsuario);
    }

    private void getAmigos() {
        GraphRequest request1 = GraphRequest.newMyFriendsRequest(token, new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray objects, GraphResponse response) {
                Type type = new TypeToken<List<ObjBasicModel>>() {
                }.getType();
                setAmigos((List<ObjBasicModel>) Utils.jsonToObject(objects.toString(), type));
            }
        });
        request1.executeAsync();
    }

    private void setAmigos(List<ObjBasicModel> list) {
        if (list.size() > 0) {
            AmigosAdapter amigosAdapter = new AmigosAdapter(list, new AmigosAdapterCallback() {
                @Override
                public void selectedAmigo(ObjBasicModel objBasicModel) {
                    listAmigosSelected.add(objBasicModel);
                }

                @Override
                public void unSelectedAmigo(ObjBasicModel objBasicModel) {
                    for (int i = 0; i < listAmigosSelected.size(); i++) {
                        if (listAmigosSelected.get(i).getId() == objBasicModel.getId()) {
                            listAmigosSelected.remove(i);
                            break;
                        }
                    }
                }
            });
            listaAmigos.setNestedScrollingEnabled(false);
            listaAmigos.setLayoutManager(new LinearLayoutManager(this));
            listaAmigos.setAdapter(amigosAdapter);
        } else {
            containerAmigos.setVisibility(View.GONE);
            txtSemAmigos.setVisibility(View.VISIBLE);
        }
    }

    private void getLocation() {
        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    getPlacesFacebook(location);
                } else {
                    materialDialog.show();
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            materialDialog.dismiss();
                            locationManager.removeUpdates(this);
                            getPlacesFacebook(location);
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {
                            materialDialog.dismiss();
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                            materialDialog.dismiss();
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            materialDialog.dismiss();
                        }
                    });
                }
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                containerLugaresProximos.setVisibility(View.GONE);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void getPlacesFacebook(Location location) {
        GraphRequest request = GraphRequest.newPlacesSearchRequest(token, location, 1000, 10, "", new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray objects, GraphResponse response) {
                Type type = new TypeToken<List<ObjBasicModel>>() {
                }.getType();
                setLugares((List<ObjBasicModel>) Utils.jsonToObject(objects.toString(), type));
            }
        });
        request.executeAsync();
    }

    private void setLugares(List<ObjBasicModel> list) {
        LugaresAdapter lugaresAdapter = new LugaresAdapter(list, new LugaresAdapterCallback() {
            @Override
            public void onSelectItem(ObjBasicModel objBasicModel) {
                selectLugar = objBasicModel;
            }
        });
        listaLugares.setNestedScrollingEnabled(false);
        listaLugares.setLayoutManager(new LinearLayoutManager(this));
        listaLugares.setAdapter(lugaresAdapter);
        containerLugaresProximos.setVisibility(View.VISIBLE);
    }

    private List<String> getListIdAmigos() {
        List<String> listIds = new ArrayList<>();
        for (ObjBasicModel obj : listAmigosSelected) {
            listIds.add(obj.getId());
        }

        return listIds;
    }

    private void verifyLocation() {
        if (Utils.isLocationEnable(this)) {
            getLocation();
        } else {
            new MaterialDialog.Builder(this)
                    .content("Percebemos que a sua localização esta desativada. Deseja ativar?")
                    .positiveText("Sim")
                    .negativeText("Não")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Utils.openConfigLocaton(HomeActivity.this);
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            containerLugaresProximos.setVisibility(View.GONE);
                        }
                    })
                    .show();
        }
    }

    @OnClick(R.id.btn_compartilhar)
    public void clickCompartilhar() {
        ShareLinkContent shareLinkContent;
        if (selectLugar != null) {
            if (listAmigosSelected.size() > 0) {
                shareLinkContent = new ShareLinkContent.Builder()
                        .setPlaceId(selectLugar.getId())
                        .setPeopleIds(getListIdAmigos())
                        .build();
            } else {
                shareLinkContent = new ShareLinkContent.Builder()
                        .setPlaceId(selectLugar.getId())
                        .build();
            }
        } else {
            if (listAmigosSelected.size() > 0){
                shareLinkContent = new ShareLinkContent.Builder()
                        .setPeopleIds(getListIdAmigos())
                        .build();
            }else{
                shareLinkContent = new ShareLinkContent.Builder()
                        .build();
            }
        }

        new ShareDialog(this).show(shareLinkContent);
    }

    @OnClick(R.id.btn_logout)
    public void clickLogou() {
        new MaterialDialog.Builder(this)
                .content("Tem certeza que deseja sair ?")
                .positiveText("Sim")
                .negativeText("Não")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginManager.getInstance().logOut();
                        finish();
                        MainActivity.open(HomeActivity.this);
                    }
                })
                .show();
    }


}

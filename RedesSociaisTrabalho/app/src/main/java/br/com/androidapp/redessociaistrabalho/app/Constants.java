package br.com.androidapp.redessociaistrabalho.app;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public class Constants {

    public static final int REQUEST_LOCATION = 293;
    public static final String URL_IMAGE_FACEBOOK = "http://graph.facebook.com/%s/picture?type=large";
}

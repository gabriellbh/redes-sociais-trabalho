package br.com.androidapp.redessociaistrabalho.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.androidapp.redessociaistrabalho.R;
import br.com.androidapp.redessociaistrabalho.app.Constants;
import br.com.androidapp.redessociaistrabalho.model.ObjBasicModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public class LugaresAdapter extends RecyclerView.Adapter<LugaresAdapter.AmigosHolder> {

    private List<ObjBasicModel> list;
    private Context context;
    private RadioButton lastRadioButton;
    private LugaresAdapterCallback lugaresAdapterCallback;

    public LugaresAdapter(List<ObjBasicModel> list, LugaresAdapterCallback lugaresAdapterCallback) {
        this.list = list;
        this.lugaresAdapterCallback = lugaresAdapterCallback;
    }

    @Override
    public AmigosHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_lugares, null);
        return new AmigosHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmigosHolder holder, int position) {
        final ObjBasicModel obj = list.get(position);
        holder.txtNomeLugar.setText(obj.getName());
        Glide.with(context)
                .load(String.format(Constants.URL_IMAGE_FACEBOOK, obj.getId()))
                .bitmapTransform(new CropCircleTransformation(context))
                .into(holder.imgLugar);
        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (lastRadioButton != null) lastRadioButton.setChecked(false);
                lastRadioButton = holder.check;
                if (lugaresAdapterCallback != null) lugaresAdapterCallback.onSelectItem(obj);
            }
        });

        holder.containerClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.check.setChecked(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class AmigosHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.check)
        RadioButton check;
        @BindView(R.id.img_lugar)
        ImageView imgLugar;
        @BindView(R.id.txt_nome_lugar)
        TextView txtNomeLugar;
        @BindView(R.id.container_click)
        LinearLayout containerClick;

        AmigosHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

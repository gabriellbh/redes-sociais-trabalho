package br.com.androidapp.redessociaistrabalho.app;

import android.app.Application;

import com.karumi.dexter.Dexter;

/**
 * Created by gabrielaraujo on 06/03/17.
 */

public class Singleton extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
    }
}

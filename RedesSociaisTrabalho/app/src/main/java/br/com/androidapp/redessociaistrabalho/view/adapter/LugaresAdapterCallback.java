package br.com.androidapp.redessociaistrabalho.view.adapter;

import br.com.androidapp.redessociaistrabalho.model.ObjBasicModel;

/**
 * Created by gabrielaraujo on 07/03/17.
 */

public interface LugaresAdapterCallback {

    void onSelectItem(ObjBasicModel objBasicModel);
}
